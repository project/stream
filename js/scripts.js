
// Close page status messages.
Drupal.behaviors.hidePageMessage = function(context) {
  $('.pane-page-messages span.icon', context).click(function() {
    $('.pane-page-messages').fadeOut('slow');
  });
}
