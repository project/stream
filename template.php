<?php

/**
 * Preprocessing the node template.
 *
 * This is basically here to unify and clean up some class names.
 */
function stream_preprocess_node(&$vars) {
  $node = $vars['node'];
  $classes = array();
  $classes[] = 'node';
  $classes[] = 'node-' . strtr($node->type, '_', '-');

  if ($node->sticky) {
    $classes[] = 'node-sticky';
  }
  if (empty($vars['classes'])) {
    $vars['classes'] = implode(' ', $classes);
  }
  else {
    $vars['classes'] = implode(' ', $classes) . ' ' . $vars['classes'];
  }
}

/**
 * Preprocessing the panel template.
 *
 * This is basically here to unify and clean up some class names.
 */
function stream_preprocess_panels_pane(&$vars) {
  $output = $vars['output'];
  // For most blocks $output->subtype is the module name plus the delta of the
  // block. We try to remove the delta here.
  $parts = explode('-', $output->subtype);
  $module = strtr(array_shift($parts), array('_' => '-'));

  if (!empty($module)) {
    // Remove the old class.
    $old_subtype = ' pane-' . strtr($output->subtype, array('_' => '-'));
    $vars['classes'] = strtr($vars['classes'], array($old_subtype => ''));
    // Add new classes.
    $vars['classes'] .= ' pane-' . $module;
  }
}

/**
 * Theme function for the page status messages. This adds a placeholder
 * for a close icon. A custom javascript is added to the page for the behavior.
 */
function stream_status_messages($display = NULL) {
  $output = '';
  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= "<div class=\"messages $type\"><span class=\"icon\"></span>\n";
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>'. $message ."</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n";
  }
  return $output;
}
